<?php
require('header.php');


?>
<form>
        <input type="text" name="search_key" placeholder="Search by"/>
        <input type="submit" value="Search"/>
</form>
<html>

        <head>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>


		</head>
		<center><h3 style="margin-top:30px">LEADS MANAGEMENT SYSTEM</h3></center>
                         <p class="container-fluid"><a class="btn btn-primary" href="add_form.php" style="margin-top:20px">Add Record</a></p>
                        <table class="table table-striped table-hover text-center" style="margin: 10px 10px 10px 2px;">


				
                                <thead style="background-color:black; color: white;">
                                        <th>SR.NO.</th>
                                        <th>FULL NAME</th>
                                        <th>CITY</th>
                                        <th>STATUS</th>
					<th>ICON</th>
					<th>ACTION</th>

                                </thead>
                                <tbody>
                                        <?php foreach($records as $row) { ?>

                                                <tr>

                                                        <td><?php echo $row['id']; ?></td>
                                                        <td><?php echo $row['full_name']; ?></td>
                                                        <td><?php echo $row['city']; ?></td>
							<td><?php echo $row['status']; ?></td>
							<td><button onclick="showSingleData(<?php echo $row['id']; ?>)"  class="btn" data-bs-toggle="modal" data-bs-target="#exampleModal"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
</svg></button></td>
                                        <td>
                                               <a href="edit_form.php?id=<?php echo $row["id"]; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
<path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
</svg></a>
                      &nbsp; |&nbsp;

                                                <a href="delete.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('Are you sure you want to delete this record?');"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
  <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
</svg></a></td>
                        </tr>
                                                       
                        </tr>




  <?php } ?>

  </table>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">user single data</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
                ID: <span id="id"></span><br>
                FULL NAME: <span id="full_name"></span><br>
                PHONE NUMBER: <span id="phone_number"></span><br>
                ALTERNATE NUMBER: <span id="alternate_phone_number"></span><br>
                WHATSAPP NUMBER: <span id="whatsapp_number"></span><br>
                EMAIL_ID: <span id="email_id"></span><br>
                City: <span id="city"></span><br>
                STATE: <span id="state"></span><br>
                PINCODE: <span id="pincode"></span><br>
                ADDED_DATE: <span id="added_date"></span><br>
                UPDATED_DATE: <span id="updated_date"></span><br>
                STATUS: <span id="status"></span><br>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
        </body>
<script>
function showSingleData(id){
        url = `getuser.php?id=${id}`
        fetch(url)
        .then(data=>data.json())
        .then(data=> {
        document.getElementById('id').innerHTML = data['id'];
        document.getElementById('full_name').innerHTML = data['full_name'];
        document.getElementById('phone_number').innerHTML = data['phone_number'];
        document.getElementById('alternate_phone_number').innerHTML = data['alternate_phone_number'];
        document.getElementById('whatsapp_number').innerHTML = data['whatsapp_number'];
        document.getElementById('email_id').innerHTML = data['email_id'];
        document.getElementById('city').innerHTML = data['city'];
        document.getElementById('state').innerHTML = data['state'];
        document.getElementById('pincode').innerHTML = data['pincode'];
        document.getElementById('added_date').innerHTML = data['added_date'];
        document.getElementById('updated_date').innerHTML = data['updated_date'];
        document.getElementById('status').innerHTML = data['status'];
        }
      );

}
</script>
</html>

